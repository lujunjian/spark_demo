import org.apache.spark.{SparkContext, SparkConf}

/**
  * Created by lujunjian1 on 2016/2/19.
  */
object Demo1 extends App {
  val conf = new SparkConf();
  conf.setMaster("local");
  conf.setAppName("Demo1");
  val sc = new SparkContext(conf);
  val lines = sc.textFile("D:\\1.csv");
  println(lines.count())
}
